import csv
import matplotlib.pyplot as plt

with open("results.csv", "r", newline = '') as resultsFile:
    
    reader = csv.DictReader(resultsFile, )
    
    data = []
    for row in reader:

        if row["TruckCount"] == "30" and row["Threshold"] == "0.0" and row["SimTime"] != "None":

            data.append(row)

    x = [float(row["MaxExtraDistance"]) for row in data]
    y = [float(row["PackageDeliveryTimeAverage"]) for row in data]

    plt.plot(x, y)

    plt.ylim([0,20])

    plt.xlabel("MaxExtraDistance")
    plt.ylabel("Μέσος χρόνος παράδοσης δεμάτων (min)")
    plt.show()
