import matplotlib.pyplot as plt

x = [30,40,50,60,70,80,90,100]
y = [9.21, 8.85, 9.69, 9.01, 9.69, 9.43, 9.70, 9.88]

plt.plot(x, y)

plt.ylim([0,20])

plt.xlabel("Πλήθος Φορτηγών")
plt.ylabel("Μέσος χρόνος παράδοσης δεμάτων")
plt.show()
