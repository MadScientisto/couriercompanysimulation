import csv
import matplotlib.pyplot as plt

with open("results.csv", "r", newline = '') as resultsFile:
    
    reader = csv.DictReader(resultsFile)
    
    data = []
    for row in reader:

        if row["TruckCount"] == "100" and row["SimTime"] != "None":

            data.append(row)

    bestTime = 10000
    bestRow = None
    for row in data:
        if float(row["PackageDeliveryTimeAverage"]) < bestTime:
            bestTime = float(row["PackageDeliveryTimeAverage"])
            bestRow = row
    
    print("best row:")
    print(bestRow)