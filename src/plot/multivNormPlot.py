import numpy as np  
import matplotlib.pyplot as plt

N = 10000000*2

mean_a = [5, 5]
cov_a = [[4, 0], [0, 4]]

Xa = np.random.multivariate_normal(mean_a, cov_a, N)
fig, ax3 = plt.subplots(nrows=1,ncols=1,figsize=(15,8))

(counts, x_bins, y_bins) = np.histogram2d(Xa[:, 0], Xa[:, 1])
ax3.contourf(counts, extent=[x_bins[0], x_bins[-1], y_bins[0], y_bins[-1]])

ax3.plot(5, 5, 'ko')
ax3.annotate("Αποθήκη", xy=(5.2,5.2))
plt.xlim(0,10)
plt.ylim(0,10)
plt.show()