import csv
import matplotlib.pyplot as plt

with open("results.csv", "r", newline = '') as resultsFile:
    
    reader = csv.DictReader(resultsFile, )
    
    data = []
    for row in reader:

        if row["MaxExtraDistance"] == "5.0" and row["Threshold"] == "0.0" and row["SimTime"] != "None":

            data.append(row)

    x = [float(row["TruckCount"]) for row in data]
    y = [float(row["PackageDeliveryTimeAverage"]) for row in data]

    plt.plot(x, y)

    plt.ylim([0,18])

    plt.xlabel("Πλήθος Φορτηγών")
    plt.ylabel("Μέσος χρόνος παράδοσης δεμάτων")
    plt.show()
