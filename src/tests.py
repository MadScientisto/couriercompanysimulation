import CourierSimulator as cs 
import csv
import time 

resultsFile =  open("results.csv", "w", newline = '')

with resultsFile:

    header = ["TruckCount", "Threshold", "MaxExtraDistance", "SimTime", "WarehouseCapacityPercentage", "PackageDeliveryTimeAverage"]
    writer = csv.DictWriter(resultsFile, fieldnames=header)

    writer.writeheader()

    testsCount = 10*10*11
    currTestCount = 0

    for truckCount in range(10,110,10):
        for threshold in [0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]:
            for maxExDist in [0.0,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0]:

                print("Checking for truckCount: {}, treshold: {}, maxExDist: {}".format(truckCount,threshold, maxExDist))

                start = time.time()

                simulator = cs.CourierSimulator(averageArrivalRate=1, maxTime=24*60, minTime = 5*60, truckCount=truckCount, 
                                                truckCapacity=200*200*200, truckCapacityThreshold=threshold, 
                                                packageMinDim=80, packageMaxDim=100, cityRadius=5, maximumExtraDistance=maxExDist, 
                                                truckWaitTime=5, warehouseCapacity=1000*1000*250)

                results = simulator.simulate()

                if not results:
                    print("Simulation not stable or warehouse full!")
                    results = ["None","None","None"]
                else:
                    print("Simulation Successful!")

                writer.writerow({"TruckCount":truckCount, 
                                "Threshold":threshold, 
                                "MaxExtraDistance":maxExDist, 
                                "SimTime":results[2], 
                                "WarehouseCapacityPercentage":results[0], 
                                "PackageDeliveryTimeAverage":results[1]})

                finish = time.time()

                print("Time Elapsed: {}".format(finish-start))

                currTestCount += 1
                print("Test Completion: {:0f}%".format((currTestCount/testsCount)*100))
                