#The code is not that beautiful... but it works. :)

import random
import time
import heapq as hp
import numpy as np
import scipy.stats as st
from functools import total_ordering
from math import log
from statistics import mean
from python_tsp.distances import euclidean_distance_matrix
from python_tsp.heuristics import solve_tsp_local_search
from python_tsp.exact import solve_tsp_dynamic_programming

@total_ordering
class Event():

    def __init__(self,eventType,time):

        self.type = eventType
        self.time = time
    
    def __eq__(self, other):
        return self.time == other.time

    def __lt__(self, other):
        return self.time < other.time


class Package():

    def __init__(self, mindim, maxdim, cityRadius, arrivalTime):

        #Volume in cm^3
        self.volume = random.randint(mindim**3,maxdim**3)
        self.arrivalTime = arrivalTime

        #Destination in kilometers
        self.x = np.random.normal(cityRadius, cityRadius/3)
        self.y = np.random.normal(cityRadius, cityRadius/3)



class Truck():

    def __init__(self, maxCapacity, capacityThreshold):

        self.packages = []
        self.maxCapacity = maxCapacity
        self.currCapacity = maxCapacity
        self.capacityThreshold = capacityThreshold
        self.distance = 0
        self.currState = "available"

    def addPackage(self, package, extraDistance):

        self.packages.append(package)
        self.currCapacity -= package.volume
        self.distance += extraDistance

class CourierSimulator():

    #Constructor
    def __init__(self, averageArrivalRate, maxTime, minTime, truckCount, truckCapacity, truckCapacityThreshold, 
                packageMinDim, packageMaxDim, cityRadius, maximumExtraDistance, truckWaitTime, warehouseCapacity):

        self.clock = 0
        self.averageArrivalRate = averageArrivalRate 
        self.maxTime = maxTime
        self.minTime = minTime
        self.truckCount = truckCount
        self.truckCapacity = truckCapacity
        self.truckCapacityThreshold = truckCapacityThreshold
        self.packageMinDim = packageMinDim
        self.packageMaxDim = packageMaxDim
        self.cityRadius = cityRadius
        self.depoX = cityRadius
        self.depoY = cityRadius
        self.maximumExtraDistance = maximumExtraDistance
        self.warehouseMaxCapacity = warehouseCapacity

        self.truckWaitTime = truckWaitTime

        #initialize empty warehouse
        self.warehouse = []
        self.warehouseCurrCapacity = self.warehouseMaxCapacity

        self.deliveredPackages = []

        #Initialize trucks with full capacity
        self.trucks = [Truck(truckCapacity, self.truckCapacityThreshold) for _ in range(truckCount)]

        #Initialize Event Queue
        self.eventQueue = []
        hp.heapify(self.eventQueue)

        #Populate Event Queue with arrivals
        self.generateArrivals()

        #Initialize stats variables
        self.last10WarehouseCap = []
        self.last10WarehouseCapMovingAverage = -1
        self.last100PackagesMovingAverage = -1

    #Generates all the package arrivals and adds them to the eventQueue
    def generateArrivals(self):
        
        currTime = 0

        while(currTime < self.maxTime):

            arrival = Event("packageArrival", currTime)
            hp.heappush(self.eventQueue, arrival)
            
            nextArrival = -log(random.random())/self.averageArrivalRate
            currTime += nextArrival

    def packageArrival(self, event):

        package = Package(self.packageMinDim, self.packageMaxDim, self.cityRadius, self.clock)

        if self.loadPackage(package):
            pass
        else:
            if self.warehouseCurrCapacity < 0:
                return False
        return True

    def loadPackage(self, package, addToWarehouse=True):
        
        #Initialize an available truck as best truck
        bestTruck = None
        bestExtraDistance = 1000000

        #Search for best truck that doesn't get over Maximum Extra Distance threshold
        for truck in self.trucks:
                
                if truck.currState == "available" and truck.currCapacity >= package.volume:

                    extraDistance = self.CalculateDistance(truck, package) - truck.distance
                    if extraDistance < bestExtraDistance and extraDistance < self.maximumExtraDistance:
                        bestExtraDistance = extraDistance
                        bestTruck = truck

        #If no such truck found, try to find empty truck
        if not bestTruck:
            for truck in self.trucks:
                if ((truck.currState == "available" and truck.currCapacity==truck.maxCapacity) 
                    or truck.currState == "departing") and truck.currCapacity >= package.volume:

                    bestTruck = truck
                    bestExtraDistance = self.CalculateDistance(bestTruck, package) - bestTruck.distance
        else:
            pass

        #If truck found load package and check if truck can depart
        if bestTruck:

            bestTruck.addPackage(package, bestExtraDistance)

            if (bestTruck.maxCapacity-bestTruck.currCapacity)/bestTruck.maxCapacity > bestTruck.capacityThreshold:

                bestTruck.currState = "departing"

                departEvent = Event("truckDeparture", self.clock+self.truckWaitTime)
                departEvent.truck = bestTruck

                hp.heappush(self.eventQueue, departEvent)

            return True

        #If no truck found, store in warehouse
        else:

            if(addToWarehouse):
                self.warehouse.append(package)
                self.warehouseCurrCapacity -= package.volume
            return False
        
    #calculate the distance the truck has to travel by adding the package
    def CalculateDistance(self, truck, package):
        
        
        #Add destinations of loaded packages to sources
        sources = [[p.x,p.y] for p in truck.packages]

        #Add warehouse location to sources
        sources.append([self.depoX,self.depoY])

        #Add package destination to sources
        sources.append([package.x,package.y])


        distance_matrix = euclidean_distance_matrix(np.array(sources))

        permutation, distance = solve_tsp_local_search(distance_matrix)

        return distance

    #When the truck reaches the capacity threshhold it prepares for departure
    def truckDeparture(self, event):
        
        truck = event.truck
        truck.currState = "delivering"

        #Truck travels with 50 km/h
        deliveryTime = (truck.distance/50)*60

        truckArrivalEvent = Event("truckArrival", self.clock+deliveryTime)
        truckArrivalEvent.truck = truck
        
        #Add to event min-heap
        hp.heappush(self.eventQueue, truckArrivalEvent)


    def truckArrival(self, event):
        
        truck = event.truck

        truck.currState = "available"

        #Handle packages
        for package in truck.packages:

            package.deliveredTime = self.clock
            self.deliveredPackages.append(package)

        #Empty truck
        truck.packages = []

        removedIndices = []
        #Load new packages
        for i in range(len(self.warehouse)):

            if self.loadPackage(self.warehouse[i], addToWarehouse=False):
                removedIndices.append(i)
        
        #Remove loaded packages from warehouse
        for ind in reversed(removedIndices):
            self.warehouseCurrCapacity += self.warehouse[ind].volume
            del self.warehouse[ind]

    def gatherStats(self):

        stableCap = False
        stablePack = False

        #Handle Warehouse capacity
        if len(self.last10WarehouseCap)<10:
            self.last10WarehouseCap.append(self.warehouseCurrCapacity)
            return False
        else:
            self.last10WarehouseCap.pop(0)
            self.last10WarehouseCap.append(self.warehouseCurrCapacity)

        if self.last10WarehouseCapMovingAverage == -1:
            self.last10WarehouseCapMovingAverage = mean(self.last10WarehouseCap)
            return False
        else:
            if abs(self.last10WarehouseCapMovingAverage-mean(self.last10WarehouseCap)) > 0.01*self.last10WarehouseCapMovingAverage:
                self.last10WarehouseCapMovingAverage = mean(self.last10WarehouseCap)
            else:
                stableCap = True

        #Handle Packages
        if len(self.deliveredPackages) >= 100:

            sumTime = 0
            for package in self.deliveredPackages[-100:]:
                sumTime += package.deliveredTime - package.arrivalTime

            meanTime = sumTime / 100

            if self.last100PackagesMovingAverage == -1:
                self.last100PackagesMovingAverage = meanTime
                return False
            else:
                if abs(self.last100PackagesMovingAverage-meanTime) > 0.05*self.last100PackagesMovingAverage:
                    self.last100PackagesMovingAverage = meanTime
                else:
                    stablePack = True
        
        if stablePack and stableCap:
            return True
        else:
            return False

            
    #Runs the simulation
    def simulate(self):
        
        while(len(self.eventQueue)>0):
            
            #Get Current Event
            currEvent = hp.heappop(self.eventQueue)
            self.clock = currEvent.time

            #Handle Package Arrival Event
            if currEvent.type == "packageArrival":

                #Check if package can be loaded on truck or stored in warehouse, else terminate simulation
                if not self.packageArrival(currEvent):
                    return None

            #Handle Truck Departure Event
            elif currEvent.type == "truckDeparture":
                self.truckDeparture(currEvent)

            #Handle Truck Arrival Event
            elif currEvent.type == "truckArrival":

                #Gather Stats before unloading packages
                #If simulation is stable return stats
                if self.gatherStats() and self.clock > self.minTime:

                    return self.last10WarehouseCapMovingAverage/self.warehouseMaxCapacity, self.last100PackagesMovingAverage, self.clock

                #Handle event
                self.truckArrival(currEvent)

        return None

def main():

    start = time.time()
    sim = CourierSimulator(averageArrivalRate=1, maxTime=24*60, minTime = 5*60, truckCount=20, truckCapacity=200*200*200, truckCapacityThreshold=0.6, 
                            packageMinDim=80, packageMaxDim=100, cityRadius=5, maximumExtraDistance=0.1, truckWaitTime=1, warehouseCapacity=1000*1000*250)
    
    res = sim.simulate()
    print("Current warehouse count:")
    print(len(sim.warehouse))
    print("Current warehouse cap:")
    print(sim.warehouseCurrCapacity)
    finish = time.time()

    print("time elapsed:")
    print(finish-start)

    print("Package volumes:")
    for package in sim.warehouse:
        print(package.volume)

    print("Results:")
    print(res)

    print("Last Package")
    print((sim.deliveredPackages[-1].arrivalTime, sim.deliveredPackages[-1].deliveredTime))

if __name__ == "__main__":

    main()




